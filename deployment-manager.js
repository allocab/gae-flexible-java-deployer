#! /usr/bin/env node

var shell = require("shelljs");
var yargs = require("yargs");
var Debug = require("./bin/debug");

var argv = yargs.usage("$0 command ")
    .command("bump-patch", "Bump to X.X.+1", function (yargs) {
        shell.exec("node "+__dirname+"/bin/bump.js -- --release=patch");
    })
    .command("bump-minor", "Bump to X.+1.0", function (yargs) {
        shell.exec("node "+__dirname+"/bin/bump.js -- --release=minor");
    })
    .command("bump-major", "Bump to +1.0.0", function (yargs) {
        shell.exec("node "+__dirname+"/bin/bump.js -- --release=major");
    })
    .command("deploy-dev","Deployment on DEV environment", function (yargs) {

        shell.exec("node "+__dirname+"/bin/deploy.js -- --env=dev");
    })
    .command("deploy-prod","Deployment on PROD environment", function (yargs) {

        shell.exec("node "+__dirname+"/bin/deploy.js -- --env=prod;node "+__dirname+"bin/bump-patch.js -- --nochangelog=true;node "+__dirname+"bin/push-all.js");
    })
    .command("test","Launch test", function (yargs) {

        shell.exec("mvn test");
    })
    .command("debug","debug string test", function (yargs) {

        Debug.debug(yargs);
    })
    .command("push-all","Push all files and push tags", function (yargs) {

        shell.exec("node "+__dirname+"/bin/push-all.js");
    })
    .command("update-changelog","Update changelog with specified message", function (yargs) {

        shell.exec("node "+__dirname+"/bin/update-changelog.js");
    })
    .demand(1, "must provide a valid command")
    .help("h")
    .alias("h", "help")
    .alias("v", "version")
    .version(function() {
        return require('./package.json').version;
    })
    .argv;