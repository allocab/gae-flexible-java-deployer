#! /usr/bin/env node
var argv = require('yargs').argv;
var Utils = require('./utils');
var shell = require('shelljs');

var version = argv.version || Utils.getCurrentVersion();
var message = argv.message;
if(!message) {
    console.log('missing message !!'.toUpperCase());
    shell.exit(1);
}
else{
    Utils.updateChangelog(version,message);
    shell.exit(0);
}
