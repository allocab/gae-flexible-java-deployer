#! /usr/bin/env node
var shell = require('shelljs');
var semver = require('semver');
var argv = require('yargs').argv;
var fs = require('fs');
var prompt = require('prompt-sync')();

//var versions = require('../changelog.json');

var versions = JSON.parse(fs.readFileSync('changelog.json','UTF-8')) || [];

var Utils = require('./utils');

var release = argv.release || 'patch';
var noChangelog = argv.nochangelog;
var message = argv.message;
console.log('release to bump', release);

var currentVersion = Utils.getCurrentVersion();
var bumpedVersion = semver.inc(currentVersion, release);
if(!bumpedVersion) {
  console.log('Version is not SEMVER'.toUpperCase());
  shell.exit(1);
}
console.log('bumped version is ',bumpedVersion);
Utils.bumpPomVersion(bumpedVersion);
console.log('pom is bumped');
Utils.execWithErrorsReport('git add pom.xml');
Utils.execWithErrorsReport('git commit -m "bump to '+bumpedVersion+'"');


if(noChangelog) {
  shell.exit(0);
}

if(message) {
  console.log('message already provided');
  Utils.updateChangelog(bumpedVersion, message);
  return;
}

var updateChangelog = prompt('Shall we update the changelog ? (N or message)');
if(!updateChangelog || updateChangelog.toLowerCase() === 'n') {
  console.log('no changelog update');
  shell.exit(0);
}
Utils.updateChangelog(bumpedVersion, updateChangelog);
