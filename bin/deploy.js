#! /usr/bin/env node
var shell = require('shelljs');
var semver = require('semver');
var argv = require('yargs').argv;
var fs = require('fs');

var versions = JSON.parse(fs.readFileSync('changelog.json', 'utf8'));
var Utils = require('./utils');

var env = argv.env || 'dev';
var project = 'allocab-'+env;

console.log('starting deploy to project ',project.toUpperCase(),'!!!');
var currentVersion = Utils.getCurrentVersion();
console.log('current version');
if(!semver.valid(currentVersion)) {
  console.log('current version in not SEMVER valid');
  return shell.exit(1);
}

var moduleName = getModuleName();

var slugifiedVersion = env+'-'+moduleName+'-v'+slugifyVersion(currentVersion);
console.log('slugifiedVersion', slugifiedVersion);

deploy(env);

function deploy(targetEnv) {
  if("dev" === targetEnv) {
    deployDev();
  }
  else if("prod" === targetEnv) {
    deployProd();
  }
  else {
    console.log('targetEnv',targetEnv,'is not valid');
    shell.exit(1);
  }
}

function getModuleName() {
  console.log('fetching module name');
  return Utils.execWithErrorsReport("mvn -q -Dexec.executable=\"echo\" -Dexec.args='${project.artifactId}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec").replace('\n','');
}

function slugifyVersion(version) {
  return version.replace(new RegExp('\\.', 'g'), '-');
}
function checkProdVersionChangelog() {
  console.log('checkProdVersionChangelog');

  var lastVersion = versions[0];
  if(lastVersion.version !== currentVersion) {
    console.log('missing changelog for version !!!!!'.toUpperCase());
    shell.exit(1);
  }
}

function proceedDeploy(env,project,slugifiedVersion) {
  Utils.execWithErrorsReport('mvn clean gcloud:deploy -P '+env+' -Dgcloud.gcloud_project='+project+'  -Dgcloud.version='+slugifiedVersion);
}

function deployDev() {
  console.log('deploying to dev, no version bump');
  proceedDeploy(env,project,slugifiedVersion);
  console.log('VERSION DEPLOYED TO DEV !!!!');
}
function deployProd() {
  checkProdVersionChangelog();
  console.log('deploying to prod, no version bump');
  Utils.execWithErrorsReport('git checkout develop');
  var pullRes = Utils.execWithErrorsReport('git pull');
  //console.log(pullRes);
  if(pullRes.stderr) {
    console.log('!!! you have unstaged changes !!!'.toUpperCase());
    shell.exit(1);
  }
  Utils.execWithErrorsReport('git checkout master');
  Utils.execWithErrorsReport('git pull');
  Utils.execWithErrorsReport('git checkout develop');
  Utils.execWithErrorsReport('git flow release start v'+currentVersion);
  proceedDeploy(env,project,slugifiedVersion);
  Utils.execWithErrorsReport('git flow release finish v'+currentVersion+' -m="'+versions[0].message+'"');
  Utils.execWithErrorsReport('git checkout develop');
  console.log('VERSION DEPLOYED TO PROD !!!!');
}

//Utils.execWithErrorsReport('git status');
