#! /usr/bin/env node
var shell = require('shelljs');
var fs = require('fs');

var Utils = {

  getCurrentVersion: function() {
    console.log('fetching current version');
    return Utils.execWithErrorsReport("mvn -q -Dexec.executable=\"echo\" -Dexec.args='${project.version}' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec").replace('\n','');
  },

  bumpPomVersion: function(version) {
    Utils.execWithErrorsReport('mvn versions:set -DnewVersion='+version);
  },

  updateChangelog: function(version, message) {
    var versions = JSON.parse(fs.readFileSync('changelog.json', 'utf8'));
    console.log('update changelog with message', message);
    if(versions[0].version === version) {
      console.log('version already in changelog, updating message');
      versions[0].message = message;
    }
    else {
      console.log('version not in changelog, inserting message');
      versions.unshift({
        version: version,
        message: message
      });
    }
    fs.writeFileSync('changelog.json', JSON.stringify(versions));
    console.log('changelog updated');
    Utils.execWithErrorsReport('git add changelog.json');
    Utils.execWithErrorsReport('git commit -m "changelog updated : '+message+'"');
  },

  execWithErrorsReport: function(command) {
    var result = shell.exec(command);
    if(result.code == 0){
        return result.stdout;
    }else{
        console.log(result.stderr);
        return null;
    }
  }

};

module.exports = Utils;
